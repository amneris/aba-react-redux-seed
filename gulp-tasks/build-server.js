var gulp = require('gulp');
var webpackConfig = require('./../webpack.server.js');
var sourcemaps = require('gulp-sourcemaps');
var stream = require('webpack-stream');
var revReplace = require('gulp-rev-replace');
var gulpif = require('gulp-if');
var injectVersion = require('gulp-inject-version');

var path = {
  ALL: ['source/server.js', 'source/newrelic.js', 'source/**/*.jsx', 'source/**/*.js'],
  DEST_BUILD_JS: 'dist'
};

module.exports = function() {

  var manifest = gulp.src('dist/rev-manifest.json');

  return gulp.src(path.ALL)
    .pipe(sourcemaps.init())
    .pipe(stream(webpackConfig))
    .pipe(sourcemaps.write())
    .pipe(gulpif(!this.opts.isLocal, revReplace({manifest: manifest})))
    .pipe(injectVersion())
    .pipe(gulp.dest(path.DEST_BUILD_JS));
};
