var gulp = require('gulp');
var livereload = require('gulp-livereload');

// configure which files to watch and what tasks to use on file changes

module.exports = function() {

  livereload.listen();

  gulp.watch([
    'source/**/*.js*',
    'source/*.js*'
  ], ['webpack']);


  gulp.watch('source/**/*.scss', ['styles']);
};
