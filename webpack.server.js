const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const Dotenv = require('dotenv-webpack');

module.exports = {
  name: 'server',
  target: 'node',
  entry: {
    server: ['./source/server.js'],
  },
  output: {
    filename: '[name].js',
    // export the bundle as a CommonJS module
    // http://webpack.github.io/docs/configuration.html#output-librarytarget
    libraryTarget: 'commonjs',
  },
  // do not touch native Node modules (e.g. fs)
  // http://webpack.github.io/docs/configuration.html#target
  externals: {
    // define newrelic as an external library
    // http://webpack.github.io/docs/configuration.html#externals
    newrelic: true,

  },
  module: {
    loaders: [{
      loader: 'babel-loader',
      test: /\.(js|jsx|es6)$/,
      exclude: /(node_modules)/,
      query: {
        plugins: ['transform-runtime'],
        presets: ['es2015', 'stage-0', 'react'],
      },
    }, {
      test: /\.json?$/,
      loader: 'json-loader',
    },
    ],
  },
  resolve: {
    extensions: ['', '.js', '.jsx', '.json'],
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        ROOT_PATH: `"${process.env.ROOT_PATH || __dirname + '/dist'}"`,
      },
    }),
    new Dotenv({
      path: process.env.NODE_ENV === 'production' ? './.env' : './.env-dev'
    }), // add all the env vars that exists in .env file
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
      },
    }),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.DedupePlugin(),
    new CopyWebpackPlugin([
      {from: './.env.json', to: './config.json'},
      {from: './source/newrelic.js', to: './newrelic.js'},
    ]),
  ],
};
