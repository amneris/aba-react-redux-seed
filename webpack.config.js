var webpack = require('webpack');
var path = require('path');

const Dotenv = require('dotenv-webpack');

module.exports = {
  cache: true,
  devtool: `"${process.env.DEV_TOOL || 'cheap-module-source-map'}"`,
  entry: {

    config: ['./source/config/appConfig.js'],
    vendor: ['react',
      'react-dom',
      'react-redux',
      'react-router',
      'redux-persist',
      'query-string',
      'react-bootstrap/lib/Panel',
      'date-utils',
      'react-scroll',
      'react-bootstrap/lib/Carousel',
      'react-bootstrap/lib/CarouselItem',
      'isomorphic-fetch'
    ],
    build: ['./source/client.js']
  },
  output: {
    filename: '[name].min.js'
  },
  module: {
    loaders: [{
      loader: 'babel-loader',
      test: /\.(js|es6)$/,
      exclude: [/(node_modules)/],
      query: {
        plugins: ['transform-runtime'],
        presets: ['es2015', 'stage-0', 'react']
      }
    }, {
      test: /\.json?$/,
      loader: 'json-loader'
    }]
  },
  resolve: {
    extensions: ['', '.js', '.json']
  },
  plugins: [
    new Dotenv({
      path: './.env'
    }), // add all the env vars that exists in .env file
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      }
    }),
    new webpack.optimize.OccurenceOrderPlugin(), //  use smallest id length for often used ids
    new webpack.optimize.DedupePlugin(), // remove duplicated files that dependecnies share
    new webpack.optimize.CommonsChunkPlugin({
      names: ['vendor', 'config']
    })
  ]
};
