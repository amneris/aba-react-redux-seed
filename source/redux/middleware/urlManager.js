import { browserHistory } from 'react-router';
import QueryString from 'query-string';

import { PAGES_ROUTES } from '../../routes';
import AppConfig from '../../config/appConfig';
import * as actionTypes from '../../actions/actionTypes';

const urlManager = store => next => action => {

  const state = store.getState();

  switch (action.type) {

    case actionTypes.FORBID_ACCESS:
      goToCampusLogin();
      break;

    case actionTypes.GO_TO_PAGE:
      goToPage(state);
      break;
  }

  return next(action);
}

function goToCampusLogin() {
  let url = AppConfig.getCampusUrl() + '/login';
  let params = {
    'returnTo' : encodeURIComponent(window.location.href)
  };

  if (Object.keys(params).length > 0) url += '?' + QueryString.stringify(params);

  window.location = url;
}

function goToPage(state) {
  let uri = '/' + state.appState.language + PAGES_ROUTES.PLANS_PAGE;
  let params = state.appState.urlParams;

  if (params.plan) delete params.plan;
  if (params.transactionId) delete params.transactionId;

  if (Object.keys(params).length > 0) uri += '?' + QueryString.stringify(params);

  browserHistory.push(uri);
}

export default urlManager;
