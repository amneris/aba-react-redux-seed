import * as actionTypes from '../../actions/actionTypes';

/**
 *
 * @param trackerSubscriber class that trackers (observers) subscribe to
 * @returns {function(*=): function(*): function(*=)}
 */
function eventTracker(trackerSubscriber) {

  // the sequence of functions below (store, next, and action) are curried
  // using es6 arrow funcions to make it easier on the eye.
  // see http://redux.js.org/docs/advanced/Middleware.html for more info
  const eventTrackerChain = store => next => action => {
    if (action.type == actionTypes.SET_USER && action.user.id) {
      trackerSubscriber.setUserId(action.user.id);
    }

    // send action to tracker subscriber
    trackerSubscriber.emitEvent(action.type, action.trackerData);

    return next(action);
  }

  return eventTrackerChain;
}

export default eventTracker;
