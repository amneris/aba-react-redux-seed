import AppConfig from './appConfig';

export default class TrackerConfig {

  static getCooladataConfig() {
    return {
      /*'cooladata': {
        key: AppConfig.getCooladataKey()
      }*/
    };
  }

  static getTrackersSetup() {
    return Object.assign({}, TrackerConfig.getCooladataConfig());
  }
}
