/**
 * provided configuration for entire app.
 * global variable as defined by webpack plugin
 */
export default class AppConfig {
  // TODO: this is not working for server side, because the process.env will always be DEV because we dont rebuild server
  // that means we will see proper appConfig in window but default value will take dev values and pixels in server side
  // will not have pro values
  static getValue(key, defaultValue){
    let returnValue;

    if (typeof window === 'object'){
      returnValue =  window.appConfig[key] || defaultValue;
    } else{
      returnValue = defaultValue
    }

    return returnValue
  }

  static getNodeEnvironment() {

    return this.getValue('NODE_ENV', process.env.NODE_ENV);
  }

  static getCampusUrl() {

    return this.getValue('CAMPUS_URL', process.env.CAMPUS_URL);
  }

  static getOauth2ClientId() {

    return this.getValue('OAUTH2_CLIENT_ID', process.env.OAUTH2_CLIENT_ID);
  }

  static getOauth2ClientSecret() {

    return this.getValue('OAUTH2_CLIENT_SECRET', process.env.OAUTH2_CLIENT_SECRET);
  }

  static getApiUrl() {

    return this.getValue('API_URL', process.env.API_URL);
  }

  static getWebSocketsUrl() {

    return this.getValue('WEBSOCKETS_URL', process.env.WEBSOCKETS_URL);
  }

}
