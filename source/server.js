var fs = require('fs');
var actuator = require('express-actuator');
const rootPath = process.env.ROOT_PATH || '';
const appConfig = JSON.parse(fs.readFileSync(path.join(rootPath, 'config.json'), 'utf8'));
import { match, RouterContext } from 'react-router';
import routes from './routes';

if (appConfig.NEW_RELIC_ENABLED) {
  require('newrelic');
}

import React from 'react';
import express from 'express';
import path from 'path';
import { renderToString } from 'react-dom/server';

import LocaleManager from './i18n/localeManager';
import Provider from './containers/provider';
import configureStore from './store/configureStore';
import { appStateInit } from './reducers/appState';
import { getDefaultLanguage, getLanguageFromUrl } from './reducers/utils';
import { createPage } from './utils/server-utils'

var compression = require('compression');

const app = express();
app.use(actuator('/management'));
app.use(compression());
const port = process.env.PORT;
const localeManager = new LocaleManager();

let language = getDefaultLanguage();

app.use('/assets', express.static(path.join(rootPath, 'assets')));

// This is fired every time the server side receives a request
app.get('*', loadApp);

app.listen(port, function () {
  console.log('Webapp listening on port ' + port);
});

function loadApp(req, res) {

  // Note that req.url here should be the full URL path from
  // the original request, including the query string.
  match({ routes, location: req.url }, (error, redirectLocation, renderProps) => {
    if (error) {
      console.log("error : 111")
      res.status(500).send(error.message)
    } else if (redirectLocation) {
      res.redirect(302, redirectLocation.pathname + redirectLocation.search)
    } else if (renderProps) {
      // You can also check renderProps.components or renderProps.routes for
      // your "not found" component or route respectively, and send a 404 as
      // below, if you're using a catch-all route.

      language = getLanguageFromUrl(req.url);

      const store = configureStore({
        appState: Object.assign({}, appStateInit, { language })
      });

      let renderedApp = renderToString(<Provider store={store} localeManager={localeManager}><RouterContext {...renderProps} /></Provider>);

      res.status(200).send(createPage(renderedApp, language, appConfig))
    } else {
      res.status(404).send('Not found')
    }
  })

}
