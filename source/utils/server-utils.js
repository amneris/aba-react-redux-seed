import React from 'react';

/**
 * Created by kbarz on 03/08/2016.
 */
export function createPage(content, language, appConfig) {

  return `
    <!DOCTYPE html>
    <html>
    <head>
      <!-- application version: %%GULP_INJECT_VERSION%% -->
      <meta charset="utf-8">
      <meta name="language" content="${language}">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
      <meta name="robots" content="noindex, nofollow, noarchive">
      <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
      <meta http-equiv="Pragma" content="no-cache" />
      <meta http-equiv="Expires" content="0" />
      <title>ABA English</title>
      <link rel="icon" href="/assets/img/favicon.ico">

      <script type="text/javascript">
        var appConfig = ${JSON.stringify(appConfig)}
      </script>

      <script type="text/javascript" src="/assets/scripts/config.min.js"/></script>

      <link rel="stylesheet" href="/assets/stylesheets/vendors.css">
      <link rel="stylesheet" href="/assets/stylesheets/app.css">
      
    </head>
    <body class="app-background">
      <div id="content">${content}</div>
      <script src="/assets/scripts/vendor.min.js"></script>
      <script src="/assets/scripts/build.min.js"></script>
    </body>
    </html>
  `;
}
