import React from 'react';
import { Route, IndexRedirect, Redirect } from 'react-router';
import App from './containers/app';
import Page from './containers/page';

export const PAGES_ROUTES = {
  PAGE : '/page'
};

export const DEFAULT_PAGE = PAGES_ROUTES.PAGE;
export const DEFAULT_LOCALE = 'en';

export default (
  <div>
    <Route component={App}>
      <IndexRedirect to={'/:lang' + PAGES_ROUTES.PAGE}/>
      <Route path={'/:lang' + PAGES_ROUTES.PAGE} component={Page}/>
    </Route>
    <Redirect from="*" to={DEFAULT_LOCALE + '/' + DEFAULT_PAGE}/>
  </div>
);
