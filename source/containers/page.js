import React from 'react';
import {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import 'date-utils';
import TestComponent from '../components/page/testComponent';
import {callActionViewPage, callActionSetAppState} from '../actions/synch';

class PlansPage extends Component {

  constructor(props) {
    super(props);

    this.state = {
      plansListState: 'loading',
      plansListData: null,
      autoScroll: null
    };
  }

  componentWillMount() {
    this.props.callActionSetAppState({
      title: this.context.localeManager.getMessage('title plans page'),
      subtitle: this.context.localeManager.getMessage('subtitle plans page')
    });

    this.props.callActionViewPage();
  }



  render() {

    return (
      <div className="page-container">
        <TestComponent />
      </div>
    );
  }
}

PlansPage.contextTypes = {
  localeManager: PropTypes.object
};

function mapStateToProps(state) {

  return {
    plans: state.plans,
    appState: state.appState || {
      title: '',
      urlParams: null
    }
  };
}

export default connect(
  mapStateToProps,
  {callActionSetAppState, callActionViewPage}
)(PlansPage);
