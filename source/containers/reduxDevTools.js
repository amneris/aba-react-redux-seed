import React from 'react';
import { createDevTools } from 'redux-devtools';
import LogMonitor from 'redux-devtools-log-monitor';
import DockMonitor from 'redux-devtools-dock-monitor';

/**
 * class to initialise Redux DevTools.
 * Redux DevTools provide a live-editing time travel environment for Redux.
 * see https://github.com/gaearon/redux-devtools for more info.
 */
export default createDevTools(
  <DockMonitor toggleVisibilityKey="ctrl-h"
               changePositionKey="ctrl-w"
               defaultPosition="bottom"
               defaultIsVisible={false}>
    <LogMonitor />
  </DockMonitor>
);
