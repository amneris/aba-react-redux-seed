import React from 'react';
import { Component, PropTypes } from 'react';

/**
 * class that acts as context provider for entire app.
 * By adding childContextTypes and getChildContext to this class (the context provider),
 * React passes the information down automatically and any component in the subtree (in this case, all app components)
 * can access it by defining contextTypes.
 */
export default class Provider extends Component {
  getChildContext() {
    return {
      localeManager: this.props.localeManager,
      store: this.props.store
    };
  }

  render() {
    // render any children components (is this case, the app itself)
    return <div>{this.props.children}</div>;
  }
}

// define the properties that this component expects to receive
Provider.propTypes = {
  store: PropTypes.object.isRequired,
  localeManager: PropTypes.object.isRequired,
  children: PropTypes.node
};

// specify the properties of this component that are to be made publicly available
// to all it's children (i.e. the entire app context)
Provider.childContextTypes = {
  localeManager: PropTypes.object,
  store: PropTypes.object
};
