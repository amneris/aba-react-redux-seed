require('array.prototype.find').shim();

import React from 'react';
import { connect } from 'react-redux';
import Loading from '../components/states/loading';
import Error from '../components/states/error';

import { getDefaultLanguage, convertLanguageToLocale } from '../reducers/utils';
import { callActionFetchLogin, callActionFetchUserInfo } from '../actions/asynch';
import { callActionSetAppState, callActionSetUrlParams } from '../actions/synch';

class App extends React.Component {

  constructor(props) {
    super(props);

    this.state =  {
      startApp: undefined
    };
  }

  componentWillReceiveProps(nextProps) {
    // when we get the user id, the app is able to start
    if (!this.props.user.id && nextProps.user.id) {
      this.setState({
        startApp: true
      });
    }
  }

  componentWillMount() {
    this._setAppLanguage();

    this.setState({
      startApp: undefined
    });
  }

  componentDidMount() {
    this.authenticateUser();
  }

  authenticateUser() {
    this.setState({
      startApp: undefined
    });

    this.props.callActionSetUrlParams(this.props.appState.urlParams);
    this.props.callActionFetchLogin(this.props.appState.urlParams) // todo use a real login module
      .then(::this.props.callActionFetchUserInfo)
      .catch(::this.setAuthenticationFailed);
  }

  setAuthenticationFailed() {
    this.setState({
      startApp: false
    });
  }

  _setAppLanguage() {
    let language = this.props.params ? this.props.params.lang :
    this.props.appState.language ||
    getDefaultLanguage();

    let localeCountry = convertLanguageToLocale(language);

    this.props.callActionSetAppState({language: language, startApp: false});

    this.context.localeManager.setMessages(localeCountry);
  }

  render() {

    return (
      <div className="app-container">
        {(() => {

          if (this.state.startApp === true) {
            return this.props.children;
          }
          else if (this.state.startApp === false) {
            return <Error onClickCallback={::this.authenticateUser} />
          }
          else {
            return <Loading />
          }

        })()}
      </div>

    );
  }
}

// define properties of app context that this component needs to access
App.contextTypes = {
  localeManager: React.PropTypes.object
};

function mapStateToProps(state, ownProps) {

  let urlParams = ownProps.location ? ownProps.location.query : undefined;

  let currentAppState = state.appState || {
      language: null,
      urlParams: null,
      startApp: false,
      title: null,

    };

  Object.assign(currentAppState, {urlParams: urlParams});

  return {
    appState: currentAppState,
    user: state.user || {
      country: null,
      currency: null,
      email: null,
      id: null,
      name: null,
      surname: null,
      userType: null
    }
  };
}

export default connect(
  mapStateToProps,
  {
    callActionSetAppState, callActionSetUrlParams, callActionFetchLogin, callActionFetchUserInfo
  }
)(App);
