import { getDefaultContactPhone } from './utils';
import {
  SET_APP_STATE, SET_URL_PARAMS
} from '../actions/actionTypes';

export const appStateInit = {
  language: null,
  urlParams: {},
  title: null
};

export function appState(state = appStateInit, action) { //NOSONAR

  switch (action.type) {
    case SET_APP_STATE:
      return Object.assign({}, state,
        action.appState
      );

    case SET_URL_PARAMS:
      return Object.assign({}, state, {
          urlParams: action.urlParams
        }
      );

    default:
      return state;
  }
}
