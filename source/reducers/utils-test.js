import expect from 'expect'

import {
  DEFAULT_LANGUAGE, DEFAULT_LOCALE, SUPPORTED_LOCALES,
  getLanguageFromUrl, getDefaultLocale, getDefaultLanguage,
  convertLanguageToLocale, startsWith
} from './utils';

describe('Convert language to locale', () => {
  it('should return the best locale depending on language', () => {
    expect(convertLanguageToLocale('es')).toBe('es-ES');
    expect(convertLanguageToLocale('ru')).toBe('ru-RU');
    expect(convertLanguageToLocale('something')).toBe(DEFAULT_LOCALE);
    expect(convertLanguageToLocale(getDefaultLanguage())).toBe(DEFAULT_LOCALE);
    expect(convertLanguageToLocale()).toBe(DEFAULT_LOCALE);
  });
});

describe('Get the language from the URL', () => {
  it('should return the best locale depending on language', () => {
    expect(getLanguageFromUrl('/it/plans')).toBe('it');
    expect(getLanguageFromUrl('/something/plans')).toBe(DEFAULT_LANGUAGE);
    expect(getLanguageFromUrl('/something')).toBe(DEFAULT_LANGUAGE);
    expect(getLanguageFromUrl('no_slash')).toBe(DEFAULT_LANGUAGE);
    expect(getLanguageFromUrl()).toBe(DEFAULT_LANGUAGE);
  });
});

describe('Get the default values', () => {
  it('should return the default value of language', () => {
    expect(getDefaultLanguage()).toBe(DEFAULT_LANGUAGE);
  });

  it('should return the default value of locale', () => {
    expect(getDefaultLocale()).toBe(DEFAULT_LOCALE);
  });
});

describe('Check if a string starts with another string', () => {
  it('should return if a string starts with another', () => {
    expect(startsWith('mytestisawesome','my')).toBeTruthy();
    expect(startsWith('mytestisawesome','mytest')).toBeTruthy();
    expect(startsWith('mytestisawesome','awesome')).toBeFalsy();
    expect(startsWith('mytestisawesome','your')).toBeFalsy();
    expect(startsWith('mytestisawesome','')).toBeFalsy();
    expect(startsWith('mytestisawesome',null)).toBeFalsy();
    expect(startsWith('mytestisawesome')).toBeFalsy();
    expect(startsWith('mytestisawesome',undefined)).toBeFalsy();
  });

  it('should return if an array elemento starts with a string', () => {
    expect(startsWith(SUPPORTED_LOCALES[0],'de')).toBeTruthy();
    expect(startsWith(SUPPORTED_LOCALES[0],'es')).toBeFalsy();
    expect(startsWith(SUPPORTED_LOCALES[0],'')).toBeFalsy();
    expect(startsWith(SUPPORTED_LOCALES[0],null)).toBeFalsy();
    expect(startsWith(SUPPORTED_LOCALES[0])).toBeFalsy();
    expect(startsWith(SUPPORTED_LOCALES[0],undefined)).toBeFalsy();
  });
});
