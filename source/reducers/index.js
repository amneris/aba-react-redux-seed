import { combineReducers } from 'redux';

import { apiAuth } from './apiAuth';
import { appState } from './appState';
import { user } from './user';

const rootReducer = combineReducers({
  apiAuth, appState, user
});

export default rootReducer;
