import {
  REQUEST_TERMS_AND_CONDITIONS, RECEIVE_TERMS_AND_CONDITIONS, REQUEST_PRIVACY_POLICY, RECEIVE_PRIVACY_POLICY
} from '../actions/actionTypes';


export function externalContent(state = {
  isFetchingTermsAndConditions: false,
  isFetchingPrivacyPolicy: false,
}, action) {
  switch (action.type) {
    case REQUEST_TERMS_AND_CONDITIONS:
      return Object.assign({}, state, {
        isFetchingTermsAndConditions: true
      });

    case RECEIVE_TERMS_AND_CONDITIONS:
      return Object.assign({}, state, {
        isFetchingTermsAndConditions: false
      });

    case REQUEST_PRIVACY_POLICY:
      return Object.assign({}, state, {
        isFetchingPrivacyPolicy: true
      });

    case RECEIVE_PRIVACY_POLICY:
      return Object.assign({}, state, {
        isFetchingPrivacyPolicy: false
      });

    default:
      return state;
  }
}
