import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import createLogger from 'redux-logger';

import TrackerConfig from '../config/trackerConfig';

import rootReducer from '../reducers';
import DevTools from '../containers/reduxDevTools';
import urlManager from '../redux/middleware/urlManager';
import eventTracker from '../redux/middleware/eventTracker';
import AbaTrackerSubscriber from 'aba-tracker-subscriber';

export default function configureStore(initialState) {

  AbaTrackerSubscriber.init(TrackerConfig.getTrackersSetup());

  let middleware = [thunkMiddleware];

  if (typeof(window) !== 'undefined') {
    middleware = [ ...middleware, createLogger(), urlManager, eventTracker(AbaTrackerSubscriber) ]
  }

  return createStore(
    rootReducer,
    initialState, // initial state only works this way when not using store persist
    compose(
      //autoRehydrate(), // to sync the local storage with the app state
      applyMiddleware(...middleware),
      DevTools.instrument())
  );
}
