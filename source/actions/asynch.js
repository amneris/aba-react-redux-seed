/**
 * Redux Thunk middleware allows you to write action creators that return a function instead of an action.
 * The thunk can be used to delay the dispatch of an action, or to dispatch only if a certain condition is met.
 * The inner function receives the store methods dispatch and getState as parameters.
 */
import AppConfig from '../config/appConfig';

import {
  setApiAuth, setUser, forbidAccess
} from './actionCreators';

import {
  apiTokenRequestByAutoLoginKey, apiTokenRequestByPassword, apiSecureRequest, apiRequest,
  ClientOAuth2HasExpired, ClientOAuth2RefreshToken
} from '../services/api';

import { appendParamsToUrl } from './utils';

export function callApiSecureRequest(url, method, params) {

  return (dispatch) => {

    return dispatch(handleAuthToken()) // ensure of having a valid token before the request
      .then(authToken => {
        return dispatch(executeApiSecureRequest(authToken, url, method, params))
      });
  };
}

export function executeApiSecureRequest(authToken, url, method, params) {

  return (dispatch) => {

    return apiSecureRequest(authToken, url, method, params)
      .catch((error) => {
        return dispatch(handleError(error));
      });
  };
}

export function handleAuthToken() {

  return (dispatch, getState) => {
    const state = getState();
    const { authToken } = state.apiAuth;

    if (!authToken) { // request for a token for the 1st time

      return dispatch(fetchAuthToken());
    }
    else if (ClientOAuth2HasExpired(authToken)) {

      return dispatch(refreshToken(authToken)); // request for a token using the refresh token
    }

    return Promise.resolve(authToken);
  };
}

export function fetchAuthToken() {

  return (dispatch, getState) => {
    const state = getState();

    const authUrl = AppConfig.getApiUrl() + '/oauth/token';
    const { email, autoLoginKey, password } = state.user;

    if (email && autoLoginKey) {
      return dispatch(fetchAuthTokenByAutoLoginKey(authUrl, email, autoLoginKey));
    }
    else {
      return dispatch(fetchAuthTokenByPassword(authUrl, email, password));
    }
  };
}

export function fetchAuthTokenByAutoLoginKey(authUrl, email, autoLoginKey) {

  return (dispatch) => {

    return apiTokenRequestByAutoLoginKey(authUrl, email, autoLoginKey)
      .then(authResponse => {
        dispatch(setApiAuth(authResponse));
        dispatch(setUser({
          id: authResponse.data.userId,
        }));

        return authResponse;
      })
      .catch((error) => {
        return dispatch(handleError(error));
      });
  }
}

export function fetchAuthTokenByPassword(authUrl, email, password) {

  return (dispatch) => {

    return apiTokenRequestByPassword(authUrl, email, password)
      .then(authResponse => {
        dispatch(setApiAuth(authResponse));
        dispatch(setUser({
          id: authResponse.data.userId,
        }));

        return authResponse;
      })
      .catch((error) => {
        return dispatch(handleError(error));
      });
  }
}

export function refreshToken(authToken) {

  return (dispatch) => {

    return ClientOAuth2RefreshToken(authToken)
      .then(authResponse => {
        dispatch(setApiAuth(authResponse));

        return authResponse;
      })
      .catch(dispatch(fetchAuthToken())); // request for a token after refresh token has also expired
  }
}

export function handleError(error) {

  return (dispatch) => {
    switch (error.status) {
      case 401:
        dispatch(forbidAccess());

      default:

        return Promise.reject(error)
          .catch((error) => {
            console.error('ERROR OCCURRED - ' + error.name + ': ' + error.message);

            return Promise.reject(); // to allow more catches in external chains
          });
    }
  }
}

/* public methods */

export function callActionFetchLogin(urlParams = {}) {
  // we want to use this function as less as possible because it means a call to abawebapps
  // that's why we add a new option that is receive the email and the autol through the app url

  return (dispatch) => {
    const { email, autol, password } = urlParams;

    if (email && (autol || password)) {
      return Promise.resolve()
        .then(() => {
          dispatch(setUser({
            autoLoginKey: autol ? decodeURIComponent(autol) : undefined,
            email: decodeURIComponent(email),
            password: password ? decodeURIComponent(password) : undefined
          }));
        });
    }
    else {
      let url = AppConfig.getCampusUrl() + '/payments/wsGetUserData';

      if (autol) {
        url = appendParamsToUrl(url, {
          autol: autol
        });
      }

      return apiRequest(url)
        .then((loginData) => {
          dispatch(setUser({
            autoLoginKey: loginData.autoLoginKey,
            email: loginData.email,
            password: loginData.password
          }));
        })
        .catch((error) => {
          return dispatch(handleError(error));
        });
    }
  }
}

export function callActionFetchUserInfo() {

  return (dispatch) => {
    // todo user an endpoint returning "myself" insted of sending the user id because oauth already knows the user
    let url = AppConfig.getApiUrl() + '/api/v1/users/265';

    return dispatch(callApiSecureRequest(url))
      .then(userJson => {
        dispatch(setUser({
          country: userJson.user.country,
          name: userJson.user.name,
          surname: userJson.user.surname,
          userType: userJson.user.userType
        }));
      });
  };
}
