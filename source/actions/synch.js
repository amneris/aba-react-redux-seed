import {
  setAppState, setUrlParams, goToPage, viewPage,
} from './actionCreators';

export function callActionSetAppState(appState) {
  return setAppState(appState);
}

export function callActionSetUrlParams(urlParams) {
  return setUrlParams(urlParams);
}

export function callActionViewPage() {
  return viewPage();
}

export function callActionGoToPage() {
  return goToPage();
}

