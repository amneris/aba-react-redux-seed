// Navigation actions
export const FORBID_ACCESS = 'FORBID_ACCESS';
export const GO_TO_PAGE = 'GO_TO_PAGE';
export const VIEW_PAGE = 'VIEW_PAGE';

// Asynchronous data actions
export const SET_USER = 'SET_USER';

// Application actions
export const SET_APP_STATE = 'SET_APP_STATE';
export const SET_URL_PARAMS = 'SET_URL_PARAMS';
export const SET_API_AUTH = 'SET_API_AUTH';
