/**
 * It is another common convention that, instead of creating action objects inline in the places where you dispatch
 * the actions, you would create functions generating them.
 */
import * as actionTypes from './actionTypes';

export function viewPage() {
  return {
    type: actionTypes.VIEW_PAGE
  };
}

export function setApiAuth(authToken) {

  return {
    type: actionTypes.SET_API_AUTH,
    authToken
  };
}

export function setAppState(appState) {
  return {
    type: actionTypes.SET_APP_STATE,
    appState
  };
}

export function setUrlParams(urlParams) {
  return {
    type: actionTypes.SET_URL_PARAMS,
    urlParams
  };
}

export function setUser(user) {

  return {
    type: actionTypes.SET_USER,
    user
  };
}

export function forbidAccess() {
  return {
    type: actionTypes.FORBID_ACCESS
  };
}


export function goToPage() {
  return {
    type: actionTypes.GO_TO_PAGE
  };
}

