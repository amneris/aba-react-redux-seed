import React from 'react';
import {Component, PropTypes} from 'react';

export default class Loading extends Component {

  constructor(props) {
    super(props);

    this.scaleFactor = props.scaleFactor || 0.2;
  }

  render() {
    var divStyle = {
      transform: `scale(${this.scaleFactor})`
    };

    return (
      <div className="aba-loading">
        <div className="loading-container">
          <div className="loading-spinner" style={divStyle}>
            <div></div>
            <div></div>
          </div>
        </div>
      </div>
    );
  }
}
