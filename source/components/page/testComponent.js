import React from 'react';
import {Component, PropTypes} from 'react';
import { connect } from 'react-redux';

import Loading from '../states/loading';
import Error from '../states/error';

import { callActionFetchUserInfo } from '../../actions/asynch';

class TestComponent extends Component {

  componentWillMount() {
    this.title = this.props.title;
    this.text = this.props.text;
    this.icon = this.props.icon;
  }

  testSecureCallToApi() {
    this.props.callActionFetchUserInfo(265);
  }

  render() {
    return (
      <div className="testComponent">
        <Loading />
        <Error onClickCallback={() => { return false }}/>
        <button className="testComponent-button" onClick={::this.testSecureCallToApi}>
          <span className="testComponent-icon base-icon-ui-outline-2_time"></span>
          <span className="testComponent-text">Call to api using security</span>
        </button>
      </div>
    );
  }
}

function mapStateToProps(state) {

  return {};
}

export default connect(
  mapStateToProps,
  {
    callActionFetchUserInfo
  }
)(TestComponent);
