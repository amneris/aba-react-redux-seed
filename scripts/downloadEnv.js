var callModule = require('./promisifiedHttp');
var fs = require('fs');
var environment = process.env.ENVIRONMENT || 'local';
//var projectName = git config --local remote.origin.url|sed -n 's#.*/\([^.]*\)\.git#\1#p'
var configServerEndpointRoot = process.env.CONFIG_SERVER_URI || 'cloud-config-server.aba.solutions';
var configServerPath = '/aba-react-seed/' + environment;


module.exports = {

  downloadConfig: function (callback) {

    configServerEndpointRoot = configServerEndpointRoot.replace('https://', '');
    configServerEndpointRoot = configServerEndpointRoot.replace('http://', '');

    var serverDetails = {
      host: configServerEndpointRoot,
      port: 80,
      path: configServerPath,
      method: 'GET'
    };

    console.log('DOWNLOADING ENV VARS FROM ' + configServerEndpointRoot + configServerPath);

    callModule.getData(serverDetails).then(function (response) {

      var config = JSON.stringify(response.propertySources[0].source);

      console.log(config);
      fs.writeFileSync('.env.json', config);

      if (fs.existsSync('./dist')) { // check required for first build of the app
        fs.writeFileSync('./dist/config.json', config);
      }

      callback();

    }).catch(function (err) {
      console.log('ERROR DOWNLOADING ENV VARS : ' + err);
    });
  }
};
